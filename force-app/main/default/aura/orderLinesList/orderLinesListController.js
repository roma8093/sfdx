({
    init: function (component, event, helper) {
        var orderLine = component.get('v.orderLine');
        if(!orderLine.Pending_Quantity__c){
            orderLine.Pending_Quantity__c = 0;
        }
    },
    
    handleChange : function (component, event) {
        var line = component.get('v.orderLine');
        var validExpense = component.find('validation').reduce(function (validSoFar, inputCmp) {
            // Displays error messages for invalid fields
            inputCmp.showHelpMessageIfInvalid();
            return validSoFar && inputCmp.get('v.validity').valid;
        }, true);
        console.log(validExpense);  //disabledCancelButton
        if(!validExpense){
            var array1 = component.get('v.validationErrorIDs');
            if(array1.indexOf(line.Id) === -1){
                array1.push(line.Id);
            }
        }else{
            var array1 = component.get('v.validationErrorIDs');
            var index = array1.indexOf(line.Id);
            if(index > -1){
                array1.splice(index, 1);
            }
        }
        console.log(array1);
        component.set('v.validationErrorIDs', array1);
        if(array1 == 0){
            component.set('v.disabledCancelButton', false);
        }else{
            component.set('v.disabledCancelButton', true);
        }
        
        var cancellationAmount = component.get('v.orderLine.Unit_Selling_Price__c')*component.get('v.orderLine.QTY_Cancelled__c')
        var orderLines = component.get('v.orderLines');
        var newOrderLines = [];
        for(var i=0; i<orderLines.length; i++){
            var record = orderLines[i];
            if(line.Id == orderLines[i].Id){
                record.Cancellation_Amount__c = cancellationAmount;
                record.QTY_Cancelled__c = component.get('v.orderLine.QTY_Cancelled__c');
            }
            newOrderLines.push(record);
        }
        component.set('v.warningType', 'slds-hide');
        component.set('v.warning', ' ');
        component.set('v.orderLines',newOrderLines);
    },
    
    changedToggle : function (component, event) {
        component.set('v.warningType', 'slds-hide');
        component.set('v.warning', ' ');
    }
})