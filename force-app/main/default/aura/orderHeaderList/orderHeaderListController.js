({
    requestButton: function (component, event, helper) {
        var action = component.get('c.isOrderRequested');
        var orderId = component.get('v.header').Id;
        action.setParams({'headerId': orderId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                var orderLinesList = response.getReturnValue();
                if(orderLinesList.length == 0){
                    component.set('v.classType', 'error');
                    component.set('v.infoMessage', 'There are no Lines in this Order Header');
                }else{
                    if (orderLinesList[0].Name == 'Another pending order'){
                        component.set('v.messageClass', ' ');
                        component.set('v.classType', 'error');
                        component.set('v.infoMessage', 'You can\'t cancel '+component.get('v.header.Name')+' Order. There is another pending cancelation order');
                    }
                    if (orderLinesList.length > 0 && orderLinesList[0].Name != 'Another pending order'){
                        
                        if(orderLinesList.length > 5){
                            component.set('v.disabledMoreLines', false);
                        }
                        component.set('v.orderLines', orderLinesList);
                        
                        component.set('v.linesPages', Math.ceil(orderLinesList.length / 5));
                        component.set('v.classType', '');
                        component.set('v.infoMessage', '');
                        component.set('v.isModalWindowOpen', true);
                    }
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    closeModalWindow: function (component, event, helper) {
        component.set('v.isModalWindowOpen', false);
        component.set('v.arrayCounter', 1);
        component.set('v.disabledLessLines', true);
    },
    
    cancelOrder: function (component, event, helper) {        
            var orderLines = component.get('v.orderLines');
            var orderLinesToCancellation = []; 
            var action = component.get('c.orderCancellation');
            for(var i=0; i<orderLines.length; i++){
                if(orderLines[i].Cancellation_Flag__c == true && orderLines[i].QTY_Cancelled__c != null && orderLines[i].QTY_Cancelled__c != '0'){
                    orderLinesToCancellation.push(orderLines[i]);
                }
                if(orderLines[i].Cancellation_Flag__c == false){
                    component.set('v.typeValue', 'Partial Order');
                }
            }        
            action.setParams({'selectedOrderHeader': component.get('v.header'), 'reasonValue': component.get('v.reasonValue'),
                              'typeValue': component.get('v.typeValue'), 'descriptionReason': component.get('v.descriptionReason'),
                              'orderLinesToCancellation': orderLinesToCancellation});
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === 'SUCCESS') {
                    if(response.getReturnValue() != 'No Line is selected or the Quantity To Be Cancelled is null'){
                        component.set('v.classType', 'success');
                        component.set('v.infoMessage', response.getReturnValue());
                        component.set('v.isModalWindowOpen', false);
                        component.set('v.warning', '');
                    }else{
                        component.set('v.warning', response.getReturnValue());
                        component.set('v.warningType', 'warning');
                    }
                }else{
                    component.set('v.classType', 'error');
                    component.set('v.infoMessage', 'Something went wrong');
                    component.set('v.warning', '');
                }
            });
            $A.enqueueAction(action);
    },
    
    reasonChanged: function (component, event, helper) {
        if(component.get('v.reasonValue') == 'Other'){
            component.set('v.hiddenClass', ' ');
        }else{
            component.set('v.hiddenClass', 'slds-hide');
        }

    },
    
    typeChanged: function (component, event, helper) {
        if(component.get('v.typeValue') == 'Full Order'){
            component.set('v.toggleDisabled', true);
        }else{
            component.set('v.toggleDisabled', false);
        }
        var orderLines = component.get('v.orderLines');
        var newOrderLines = [];
        for(var i=0; i<orderLines.length; i++){
            var record = orderLines[i];
            if(component.get('v.typeValue') == 'Full Order'){
                record.Cancellation_Flag__c = true;
            }else{
                record.Cancellation_Flag__c = false;
            }
            newOrderLines.push(record);
            
            component.set('v.orderLines',newOrderLines);
        }
    },
    
    showMoreLines: function (component, event, helper) {
        var arrayCounter = component.get('v.arrayCounter');
        component.set('v.arrayCounter', arrayCounter+1);
       /* var orderLinesFiveRecords = [];
        var countOfRecords;
        if(5*(arrayCounter+1) >= component.get('v.orderLinesStorage').length){
            countOfRecords = component.get('v.orderLinesStorage').length;
        }else{
            countOfRecords = 5*(arrayCounter+1);
        }
           
        for(var i = 0; i < countOfRecords; i++) {
           // console.log(i+(5*arrayCounter));
            orderLinesFiveRecords.push(component.get('v.orderLinesStorage')[i]);
        } */
        component.set('v.start', (arrayCounter*5));
        component.set('v.end', (arrayCounter+1)*5);

        if(arrayCounter == component.get('v.linesPages')-1){
            component.set('v.disabledMoreLines', true);
            component.set('v.disabledLessLines', false);
            
        }else{
            component.set('v.disabledLessLines', false);
        }
    },
    // disabledMoreLines disabledLessLines
    showLessLines: function (component, event, helper) {
        var arrayCounter = component.get('v.arrayCounter');
        component.set('v.arrayCounter', arrayCounter-1);
        component.set('v.start', ((arrayCounter-2)*5));
        component.set('v.end', (arrayCounter-1)*5);
        if(arrayCounter-1 == 1){            
            component.set('v.disabledMoreLines', false);
            component.set('v.disabledLessLines', true);
        }else{
            component.set('v.disabledMoreLines', false);
        }
    }
})