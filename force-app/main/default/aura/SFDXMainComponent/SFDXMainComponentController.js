({
    init: function (component, event, helper) {                    
        helper.getHeaders(component, helper);
        helper.fetchReasonPicklist(component);
        helper.fetchTypePicklist(component);
    },
    
    searchButton : function(component, event, helper) {
        helper.getPositions(component, helper);
    },
    
    resetButton : function(component, event, helper) {
        component.set("v.orderedDate", null);
            component.set("v.customerNumber", null);
            component.set("v.factoryNumber", null);
            component.set("v.orderNumber", null);
        helper.getHeaders(component, helper);
    },
    
    onNext : function(component, event, helper) {        
        var pageNumber = component.get("v.currentPageNumber");
        component.set("v.currentPageNumber", pageNumber+1);
        helper.buildData(component, helper);
    },
    
    onPrev : function(component, event, helper) {        
        var pageNumber = component.get("v.currentPageNumber");
        component.set("v.currentPageNumber", pageNumber-1);
        helper.buildData(component, helper);
    },
    
    processMe : function(component, event, helper) {
        component.set("v.currentPageNumber", parseInt(event.target.name));
        helper.buildData(component, helper);
    },
    
    onFirst : function(component, event, helper) {        
        component.set("v.currentPageNumber", 1);
        helper.buildData(component, helper);
    },
    
    onLast : function(component, event, helper) {        
        component.set("v.currentPageNumber", component.get("v.totalPages"));
        helper.buildData(component, helper);
    },
    
    handleChange: function (component, event, helper) {
        var allData = component.get("v.allData")
        var newValue = event.getParam("value")
        component.set("v.pageSize", newValue);
        component.set("v.allData", component.get("v.allData"));
        console.log(component.get("v.allData").length+'------'+newValue);
        component.set("v.totalPages", Math.ceil(allData.length/newValue));        
        component.set("v.currentPageNumber",1);
        
        helper.buildData(component, helper);
     //   helper.getPositions(component, helper);
    }
})