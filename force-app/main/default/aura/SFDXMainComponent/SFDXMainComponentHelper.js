({
	getHeaders : function(component, helper) {
        var action = component.get("c.getHeaders");
   //     action.setStorable();
        action.setCallback(this,function(response) {
            console.log("response.getState()------"+response.getState())
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.allData", response.getReturnValue());
                component.set("v.totalPages", Math.ceil(response.getReturnValue().length/component.get("v.pageSize")));
                component.set("v.currentPageNumber",1);
                helper.buildData(component, helper);
            }                           
        });
        $A.enqueueAction(action);
    },
    
    buildData : function(component, helper) {
        var data = [];
        var pageNumber = component.get("v.currentPageNumber");
        var pageSize = component.get("v.pageSize");
        var allData = component.get("v.allData");
        var x = (pageNumber-1)*pageSize;
        //creating data-table data
        for(; x<(pageNumber)*pageSize; x++){
            if(allData[x]){
            	data.push(allData[x]);
            }
        }
       console.log(data.length);
        component.set("v.allHeaders", data);
        
        helper.generatePageList(component, pageNumber);
    },
    
    generatePageList : function(component, pageNumber){
        pageNumber = parseInt(pageNumber);
        var pageList = [];
        var totalPages = component.get("v.totalPages");
        if(totalPages > 1){
            component.set("v.styleText", "display: inline");
            if(totalPages <= 10){
                var counter = 2;
                for(; counter < (totalPages); counter++){
                    pageList.push(counter);
                } 
            } else{
                if(pageNumber < 5){
                    pageList.push(2, 3, 4, 5, 6);
                } else{
                    if(pageNumber>(totalPages-5)){
                        pageList.push(totalPages-5, totalPages-4, totalPages-3, totalPages-2, totalPages-1);
                    } else{
                        pageList.push(pageNumber-2, pageNumber-1, pageNumber, pageNumber+1, pageNumber+2);
                    }
                }
            }
        }else{
            component.set("v.styleText", "display: none");
        }
        component.set("v.pageList", pageList);
    },
    
    getPositions : function(component, helper) {      
        var searchDate = component.get('v.orderedDate');
        var stringOfDate; 
        var action = component.get('c.searchResults');
          if(searchDate == null){
             stringOfDate = 'null';
          }else{
              stringOfDate = searchDate.toString();
          }
        action.setParams({'orderNumber': component.get('v.orderNumber'), 'factoryNumber': component.get("v.factoryNumber"),
                          'customerNumber': component.get('v.customerNumber'), 'orderedDate' : stringOfDate});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                component.set("v.pageList", response.getReturnValue().length);
                component.set("v.totalPages", Math.ceil(response.getReturnValue().length/component.get("v.pageSize")));
                component.set("v.allData", response.getReturnValue());
                component.set("v.currentPageNumber",1);
                helper.buildData(component, helper);
            }
        });
        $A.enqueueAction(action);
    },
    
    fetchReasonPicklist : function(component) {
        var action = component.get("c.getPicklistvalues");
        action.setParams({
            'objectName': 'Order_Header__c',
            'field_apiname': 'Cancellation_Reason__c',
            'nullRequired': true // includes --None--
        });
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === "SUCCESS"){
                component.set("v.reasonPicklist", a.getReturnValue());
            } 
        });
        $A.enqueueAction(action);
        
    },	
    
    fetchTypePicklist : function(component){
        var action = component.get("c.getPicklistvalues");
        action.setParams({
            'objectName': 'Order_Header__c',
            'field_apiname': 'Cancellation_Types__c',
            'nullRequired': true // includes --None--
        });
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === "SUCCESS"){
                component.set("v.typePicklist", a.getReturnValue());
            } 
        });
        $A.enqueueAction(action);
    }
})