public with sharing class CancellationRequestCreation {
    @AuraEnabled
    public static List<Order_Lines__c> isOrderRequested (String headerId) {
        System.debug(headerId);
        List<Order_Lines__c> rellatedOrderLines = new List<Order_Lines__c>();
        List<Cancellation_Order_Header__c> cancelledOrders = [SELECT ID FROM Cancellation_Order_Header__c WHERE Order_Header__c =: headerId];
        if(cancelledOrders.size() > 0){
            Order_Lines__c replyOrder = new Order_Lines__c(Name = 'Another pending order', Line_Number__c = '111-222', Item_Description__c = 'There is another pending cancelation order');
            rellatedOrderLines.add(replyOrder);
        }else{
            rellatedOrderLines = [SELECT ID, Name, Line_Number__c, Item_Code__c, Item_Description__c, UOM__c, Unit_Selling_Price__c, Pending_Quantity__c,
                                         QTY_Cancelled__c, Cancellation_Amount__c, Cancellation_Flag__c FROM Order_Lines__c WHERE Order_Header__c =: headerId ORDER BY Line_Number__c];
        }
        return rellatedOrderLines;
    }
    
    @AuraEnabled
    public static String orderCancellation (Order_Header__c selectedOrderHeader, String reasonValue, String typeValue,
                                            String descriptionReason, List<Order_Lines__c> orderLinesToCancellation) {
        String response = null;
        if(orderLinesToCancellation.isEmpty()){
            response = 'No Line is selected or the Quantity To Be Cancelled is null';
        }else{
            Cancellation_Order_Header__c cancellationOrderHeader = new Cancellation_Order_Header__c(Name = selectedOrderHeader.Name,
                                                                                                   Cancellation_Reason__c = reasonValue,
                                                                                                   Cancellation_Type__c = typeValue,
                                                                                                   Currency__c = selectedOrderHeader.Currency__c,
                                                                                                   Customer_Name__c = selectedOrderHeader.Customer_Name__c,
                                                                                                   Customer_Number__c = selectedOrderHeader.Customer_Number__c,
                                                                                                   Notes__c = descriptionReason,
                                                                                                   Order_Header__c = selectedOrderHeader.Id,
                                                                                                   Order_Number__c = selectedOrderHeader.Order_Number__c,
                                                                                                   Order_Type__c = selectedOrderHeader.Order_Type__c,
                                                                                                   Ordered_Date__c = selectedOrderHeader.Ordered_Date__c,
                                                                                                   Status__c='REQUESTED');
            insert cancellationOrderHeader;

            List<Cancellation_Order_Lines__c> listOrderLines = new List<Cancellation_Order_Lines__c>();
            for(Integer f = 0; f<orderLinesToCancellation.size(); f++){
                Cancellation_Order_Lines__c createCancellationLine = new Cancellation_Order_Lines__c(Cancellation_Amount__c = orderLinesToCancellation[f].Cancellation_Amount__c,
                                                                                                    Cancellation_Order_Header__c = cancellationOrderHeader.Id,
                                                                                                    Name = orderLinesToCancellation[f].Name,
                                                                                                    Cancellation_Quantity__c = orderLinesToCancellation[f].QTY_Cancelled__c,
                                                                                                    Item_Code__c = orderLinesToCancellation[f].Item_Code__c,
                                                                                                    Item_Description__c = orderLinesToCancellation[f].Item_Description__c,
                                                                                                    Unit_Selling_Price__c = orderLinesToCancellation[f].Unit_Selling_Price__c);
                listOrderLines.add(createCancellationLine);
            }
            insert listOrderLines;
            response = 'Cancellation Header № '+selectedOrderHeader.Order_Number__c+' with Cancellation Lines was Created Successfully';
        }
        return response;
    }        
}