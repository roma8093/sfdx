@isTest
public class CancellationRequestCreationTest {
    @isTest
    static void isOrderRequestedTest() {    
        ControllerSFDXTest.setupTestData();
        String headerId = [SELECT Id FROM Order_Header__c WHERE Order_Number__c LIKE '1%' Limit 1].Id;
        List<Order_Lines__c> getOrderLines1 = CancellationRequestCreation.isOrderRequested(headerId);
        System.assertEquals(getOrderLines1.size(), 20);
        
        headerId = [SELECT Id FROM Order_Header__c WHERE Order_Number__c LIKE '2%' Limit 1].Id;
        List<Order_Lines__c> getOrderLines2 = CancellationRequestCreation.isOrderRequested(headerId);
        System.assertEquals(getOrderLines2[0].Name, 'Another pending order');
    }
    
    @isTest
    static void orderCancellationTest() {
        ControllerSFDXTest.setupTestData();
        Order_Header__c selectedOrderHeader = [SELECT Id, Name, Order_Number__c, Factory_Number__c, Customer_Number__c, Customer_Name__c, Ordered_Date__c, Order_Type__c,
            Creation_Date__c, Currency__c, Sales_Person__c FROM Order_Header__c WHERE Order_Number__c LIKE '1%' Limit 1];
        String reasonValue = 'Administrative Reason';
        String typeValue = 'Full Order';
        String descriptionReason = 'Some custom description';
        List<Order_Lines__c> orderLinesToCancellation = new List<Order_Lines__c>();
        String requestResult1 = CancellationRequestCreation.orderCancellation (selectedOrderHeader, reasonValue, typeValue, descriptionReason, orderLinesToCancellation);
        System.assertEquals(requestResult1, 'No Line is selected or the Quantity To Be Cancelled is null');
        
        orderLinesToCancellation = [SELECT ID, Name, Line_Number__c, Item_Code__c, Item_Description__c, UOM__c, Unit_Selling_Price__c,
                                                         Pending_Quantity__c, QTY_Cancelled__c, Cancellation_Amount__c, Cancellation_Flag__c
                                                         FROM Order_Lines__c
                                                         WHERE Order_Header__c =: selectedOrderHeader.Id
                                                         ORDER BY Line_Number__c];
        String requestResult2 = CancellationRequestCreation.orderCancellation(selectedOrderHeader, reasonValue, typeValue, descriptionReason, orderLinesToCancellation);
        System.assertEquals(requestResult2, 'Cancellation Header № '+selectedOrderHeader.Order_Number__c+' with Cancellation Lines was Created Successfully');
        
    }
}