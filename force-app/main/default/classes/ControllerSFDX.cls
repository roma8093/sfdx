public with sharing class ControllerSFDX {
    @AuraEnabled
    public static List<Order_Header__c> getHeaders() {        
        return [SELECT Id, Name, Order_Number__c, Factory_Number__c, Customer_Number__c, Customer_Name__c, Ordered_Date__c, Order_Type__c,
            Creation_Date__c, Currency__c, Sales_Person__c FROM Order_Header__c Where OwnerId = :userinfo.getUserId() ORDER BY Name];
    }
       
    @AuraEnabled
    public static List<Order_Header__c> searchResults (String orderNumber, String factoryNumber, String customerNumber, String orderedDate) {
        Date convertedDate;
        if(orderedDate == 'null'){
            convertedDate = null;
        }else{
            System.debug(orderedDate);
            convertedDate = Date.valueOf(orderedDate);
        }
        String queryString = SearchHelper.headersFilter(orderNumber, factoryNumber, customerNumber, convertedDate);
        System.debug(queryString);
        List<Order_Header__c> query = Database.query(queryString);            
        return query;
    }
    
    @AuraEnabled
    public static List<String> getPicklistvalues(String objectName, String field_apiname, Boolean nullRequired){
        List<String> optionlist = new List<String>();
        System.debug(objectName);
        Map<String,Schema.SObjectType> gd = Schema.getGlobalDescribe();
        System.debug(gd.get(objectName.toLowerCase()).getDescribe().fields.getMap());
        Map<String, Schema.SObjectField> field_map = gd.get(objectName.toLowerCase()).getDescribe().fields.getMap(); 
        System.debug(field_apiname);
        List<Schema.PicklistEntry> picklistValues = field_map.get(field_apiname).getDescribe().getPickListValues();
        
        if(nullRequired == true){
            optionlist.add('---');
        }
        
        for (Schema.PicklistEntry pv : picklistValues) {
            optionlist.add(pv.getValue());
        }
        System.debug(optionlist);
        return optionlist;
    }
}