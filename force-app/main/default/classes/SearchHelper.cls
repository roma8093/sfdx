public with sharing class SearchHelper {
    
    public static String headersFilter(String orderNumber, String factoryNumber, String customerNumber, Date convertedDate){
       
        List<String> res1 = new List<String>();
        
        String query = 'SELECT Id, Name, Order_Number__c, Factory_Number__c, Customer_Number__c, Customer_Name__c, Ordered_Date__c, Order_Type__c,'+
            'Creation_Date__c, Currency__c, Sales_Person__c FROM Order_Header__c';
        res1 = nameSearch(orderNumber, res1);
        res1 = dateSearch(convertedDate, res1);
        res1 = factorySearch(factoryNumber, res1);
        res1 = customerSearch(customerNumber, res1);
        
        if(res1.size() != 0){
            query = query + ' WHERE OwnerID=\''+UserInfo.getUserId()+'\' AND ';
            for(Integer i = 0; i < res1.size(); i++){
                if(i > 0){
                    query = query + ' AND ';
                }
                query = query + res1[i];
            }
        }
        query = query + ' ORDER BY Order_Number__c';
        System.debug(query);
        return query;
    }
    
    public static List<String> nameSearch(String orderNumber, List<String> res1){
        
        if(String.isNotBlank(orderNumber)){	
            
            res1.add('Order_Number__c LIKE \'' + orderNumber + '%\' ');
            System.debug('orderNumber'+orderNumber);
        }
        return res1;
    }
    
    public static List<String> customerSearch(String customerNumber, List<String> res1){
        
        if(String.isNotBlank(customerNumber) && customerNumber != null){
            System.debug('customerNumber___'+customerNumber);
            res1.add('Customer_Number__c LIKE \'' + customerNumber + '%\' ');	
        }
        return res1;
    }
    
    public static List<String> dateSearch(Date searchDate, List<String> res1){
        System.debug('searchDate!!!!!!!!!'+searchDate);
        if(searchDate != null){	
            Date myDate = date.newinstance(searchDate.year(), searchDate.month(), searchDate.day());
            String stringDate = String.valueOf(myDate);
            res1.add('Ordered_Date__c = ' + stringDate + ' ');	
        }
        return res1;
    }
    public static List<String> factorySearch(String factoryNumber, List<String> res1){
        if(String.isNotBlank(factoryNumber) && factoryNumber != null){
            
            res1.add('Factory_Number__c Like \'' + factoryNumber + '%\' ');
            
        }
        system.debug(res1);
        return res1;
    }
}