@isTest
public class ControllerSFDXTest {
    @TestSetup
    public static void setupTestData(){
        List<Order_Header__c> orderHeaderList = new List<Order_Header__c>();
        for(Integer i = 1; i<6; i++){
            Order_Header__c orderHeader = new Order_Header__c(Name = 'Name-'+i,
                                                              Currency__c = 'USD',
                                                              Factory_Number__c = i+i+i+' Factory',
                                                              Customer_Name__c = i+'/Name',
                                                              Customer_Number__c = 'Number_'+i,
                                                              Ordered_Date__c = Date.today(),
                                                              Order_Number__c = i+'Order Number',
                                                              Order_Type__c = 'Order Type');
            orderHeaderList.add(orderHeader);
        }
        List<Order_Lines__c> orderLinesList = new List<Order_Lines__c>();
        insert orderHeaderList;
        System.debug(orderHeaderList[0].Id);
        for(Integer i = 1; i<21; i++){
            Order_Lines__c orderLine = new Order_Lines__c(Order_Header__c = orderHeaderList[0].Id,
                                                          Name = 'Name'+i,
                                                          Item_Code__c = 'Item_Code',
                                                          Pending_Quantity__c = i*10,
                                                          Unit_Selling_Price__c = i*100);
            orderLinesList.add(orderLine);
        }
        Cancellation_Order_Header__c cancellationOrderHeader = new Cancellation_Order_Header__c(Name = orderHeaderList[1].Name,
                                                                                                   Currency__c = orderHeaderList[1].Currency__c,
                                                                                                   Customer_Name__c = orderHeaderList[1].Customer_Name__c,
                                                                                                   Customer_Number__c = orderHeaderList[1].Customer_Number__c,
                                                                                                   Order_Header__c = orderHeaderList[1].Id,
                                                                                                   Order_Number__c = orderHeaderList[1].Order_Number__c,
                                                                                                   Order_Type__c = orderHeaderList[1].Order_Type__c,
                                                                                                   Status__c='REQUESTED');
        
        insert orderLinesList;
        insert cancellationOrderHeader;
    }
    
    @isTest
    static void getHeadersTest() {
        List<Order_Header__c> orderHeaderList = ControllerSFDX.getHeaders();
        System.assertEquals(orderHeaderList.size(), 5);
    }
    
    @isTest
    static void searchResultsTest() {
        String orderNumber = '1';
        String factoryNumber = '3 Factory';
        String customerNumber = 'Number_1';
        String orderedDate = datetime.now().format('yyyy-MM-dd');
        List<Order_Header__c> orderHeaderList1 = ControllerSFDX.searchResults (orderNumber, factoryNumber, customerNumber, orderedDate);
        System.assertEquals(orderHeaderList1.size(), 1);
        
        orderNumber = '';
        factoryNumber = '';
        customerNumber = 'Number';
        orderedDate = 'null';
        List<Order_Header__c> orderHeaderList2 = ControllerSFDX.searchResults (orderNumber, factoryNumber, customerNumber, orderedDate);
        System.assertEquals(orderHeaderList2.size(), 5);
    }
    
    @isTest
    static void getPicklistTest() {
        String objectName = 'Order_Header__c';
        String field_apiname = 'Cancellation_Reason__c';
        Boolean nullRequired = true;
        List<String> reasonPicklist = ControllerSFDX.getPicklistvalues(objectName, field_apiname, nullRequired);
        System.assertEquals(reasonPicklist[1], 'Administrative Reason');
    }
    /* String searchStr = 'Test JobAdvertisement';
        String salaryValue = '50K-75K';
        Date searchDate = date.today();
        Test.startTest();    
        String response = SearchSiteHelper.vacanciesFilter(searchStr, searchDate, salaryValue);
        List<Job_Advertisement__c> results = Database.query(response);        
        Test.stopTest();
        
        System.debug(results);
        System.assertEquals(results.size(), 1);
        System.assertEquals(results[0].Name, 'Test JobAdvertisement 111');   */ 
}